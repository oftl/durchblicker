FROM base/archlinux

RUN pacman --sync --refresh --sysupgrade --noconfirm \
 && pacman --sync --noconfirm \
    zbar \
    python \
    python-beautifulsoup4 \
    patch \
    nodejs

WORKDIR root
COPY src /root/
CMD ["bash", "doit.sh"]
