## HowTo

Run locally: `bash doit.sh`

Or, if you are concerned about security and have a moment: build a docker image with `docker build --tag durckblicker:latest .` and run a container off of it `docker run --rm durckblicker:latest`

## What is going on

Basically `doit.sh` fetches the given QR-code, decodes it, decodes its payload from base64, reverses it, fetches the script from jsfiddle, then `extract_script.py` gets the interesting `<script>` tag out and just like with any respectable cooking show a diff is already prepared to patch the somewhat buggy JavaScript file to actually compile. Finally that is run through `node`.
