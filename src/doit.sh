/usr/bin/curl http://i.imgur.com/Xg8Ue82.png --output mystery.png
/usr/bin/zbarimg  --quiet mystery.png | sed s/^QR-Code:// > mystery.txt
/usr/bin/base64 --decode mystery.txt | rev > mystery.url
fiddleId=`sed s_http://jsfiddle.net/__ mystery.url`
/usr/bin/curl "http://fiddle.jshell.net/${fiddleId}/show/" -H "Referer: http://fiddle.jshell.net/${fiddleId}/" --output fiddle.html
/usr/bin/python extract_script.py
# prepared diff like so:
# /usr/bin/diff script.js script-pretty.js > script.diff
/usr/bin/patch script.js script.diff
/usr/bin/node script.js
