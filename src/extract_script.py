from bs4 import BeautifulSoup

soup = BeautifulSoup(open("fiddle.html", "r"), "html.parser")
script = soup.find_all("script")[1]
with (open("script.js", "w")) as fh:
    fh.write(str(script))
